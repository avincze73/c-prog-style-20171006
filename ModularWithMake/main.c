/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2017. október 6., 11:20
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PrintUtil.h"
#include "StringUtil.h"

/*
 * 
 */
int main(int argc, char** argv) {
    char* input = "abcddcba";
    char buffer[100] = {};
    strcpy(buffer, input);
    strcat(buffer, is_mirror(input)?" is a ":"is not a ");
    strcat(buffer, "mirror word");
    print_h(buffer);
    getchar();
    return (EXIT_SUCCESS);
}

