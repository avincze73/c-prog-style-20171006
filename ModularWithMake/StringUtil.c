/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StringUtil.c
 * Author: avincze
 * 
 * Created on 2017. október 6., 11:21
 */

#include "StringUtil.h"
#include <string.h>
bool is_mirror(const char* word)
{
    int i, j;
    for(i = 0,j = strlen(word) - 1; i < j; i++, j--)
    {
        if(word[i] != word[j])
        {
            return false;
        }
    }
    return true;
}